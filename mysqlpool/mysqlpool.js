const express = require('express');
const mysql = require('mysql');

const mySqlConfig = {
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'spritle',
    handleDisconnects: true
}

var connection;

const handleDisconnect = () => {

    connection = mysql.createPool(mySqlConfig);

    connection.getConnection((err) => {
        if (err) {
            console.log('MYSQL CONNECTION ERROR', err.stack);
            setTimeout(handleDisconnect, 2000);
        } else {
            console.log('MYSQL CONNECTION ESTABLISHED!');
        }
    });

    connection.on('error', (err) => {
        console.log('db error: ', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.log('MySql reconnection is initiated..');
            handleDisconnect();
        } else {
            console.log('Error in reconnection ', err);
        }
    });

    return {
        mySql_connection: connection
    }
}

module.exports = handleDisconnect();