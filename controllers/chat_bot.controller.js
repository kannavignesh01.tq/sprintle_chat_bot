const mysql_pool = require('../mysqlpool/mysqlpool');

const chat_bot_controller = () => {
    const _insert_questions_and_answers = async (req, res) => {
        try {

            const { user_id, question } = req.body;

            const insertQuery = `INSERT INTO chat_bot_user_table ( user_id, question ) VALUES ?`;

            const values = [user_id, question];

            let valueArr = [];
            valueArr.push(values);

            mysql_pool.mySql_connection.query(insertQuery, [valueArr], (err, rows, fields) => {
                try {
                    if (err) {
                        res
                            .status(200)
                            .send({
                                "message": "question and answers not inserted!",
                                "data": err
                            })
                    } else {
                        res
                            .status(200)
                            .send({
                                "message": "question and answers inserted successfully!",
                                "data": rows
                            })
                    }
                } catch (error) {
                    res
                        .status(500)
                        .send({
                            "message": "something went wrong in insertion!",
                            "error": error
                        })
                }
            });

        } catch (error) {
            res
                .status(500)
                .send({
                    "message": "something went wrong!",
                    "error": error
                });
        }
    }

    return {
        insert_questions_and_answers: _insert_questions_and_answers,
    }
}

module.exports = chat_bot_controller();