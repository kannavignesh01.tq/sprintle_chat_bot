var express = require('express');
const swaggerUiExpress = require('swagger-ui-express');

var mysqlPool = require('../mysqlpool/mysqlpool');
var swagger_file = require('../config/swagger.json');
var chat_bot_route = require('../routes/chat_bot.routes');

var router = express.Router();

router.use('/api-docs',swaggerUiExpress.serve, swaggerUiExpress.setup(swagger_file));
router.use('/chat_bot', chat_bot_route);

module.exports = router;