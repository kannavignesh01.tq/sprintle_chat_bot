var express = require('express');
var chat_bot_controller = require('../controllers/chat_bot.controller');
var chat_bot_route = express.Router();

chat_bot_route.route('/insertchatbot').post(chat_bot_controller.insert_questions_and_answers);

module.exports = chat_bot_route;