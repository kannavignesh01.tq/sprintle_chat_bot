const express = require('express');
const mysql = require('mysql');
const { serve } = require('swagger-ui-express');
const app = require('../SPRITLE/routes/service.routes');
const PORT = process.env.PORT ? 8080 : 8080;
const router = express.Router();

var server = require('http').createServer(app);

if (!module.parent) {
    server.listen(() => {
        console.log('server started on port : ', PORT)
    })

}

module.exports = router;